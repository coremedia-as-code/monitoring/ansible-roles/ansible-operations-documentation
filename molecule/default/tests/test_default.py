import pytest
import os
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture()
def AnsibleDefaults():
    with open("../../defaults/main.yml", 'r') as stream:
        return yaml.load(stream)


@pytest.mark.parametrize("dirs", [
    "/var/www/algernon",
    "/var/www/algernon/operations",
    "/var/www/algernon/operations/fonts",
    "/var/www/algernon/operations/styles",
    "/var/www/algernon/operations/themes",
])
def test_directories(host, dirs):
    d = host.file(dirs)
    assert d.is_directory
    assert d.exists


@pytest.mark.parametrize("files", [
    "/var/www/algernon/operations/index.md",
])
def test_files(host, files):
    f = host.file(files)
    assert f.exists
    assert f.is_file
